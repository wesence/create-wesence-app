import React from 'react';
import { Switch, Route } from 'react-router';
import HomePage from './home';

const Pages = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
  </Switch>
);

export default Pages;
