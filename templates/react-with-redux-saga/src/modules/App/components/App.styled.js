import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 5rem;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
    'Helvetica Neue', Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
    'Segoe UI Symbol', 'Noto Color Emoji';
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: ${({ theme }) => theme.colors.primaryText};
  text-align: left;
`;

const H1 = styled.h1`
  font-family: inherit;
  font-weight: 500;
`;

export { Wrapper, H1 };
