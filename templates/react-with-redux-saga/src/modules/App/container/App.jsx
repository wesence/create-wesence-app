// @flow
import * as React from 'react';
import { connect } from 'react-redux';

import { initRequest } from '../store/actions';
import App from '../components/App';

type Props = {
  dispatch: (Function) => void,
  app: string,
};

class AppContainer extends React.Component<Props> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(
      initRequest(
        'Quick setup for new performance orientated, offline–first React.js applications.',
      ),
    );
  }

  render() {
    const { app } = this.props;
    return <App message={app} />;
  }
}

const mapStateToProps = (state) => ({ app: state.app });

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppContainer);
