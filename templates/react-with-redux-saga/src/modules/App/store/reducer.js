import { actions } from './actions';

const INITIAL_STATE = '';

export default function appReducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case actions.INIT_SUCCESS:
      return payload;
    default:
      return state;
  }
}
