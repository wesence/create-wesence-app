import actionCreator from 'utils/actionCreator';

export const actions = actionCreator('app', ['INIT_REQUEST', 'INIT_SUCCESS']);

export function initRequest(payload) {
  return {
    type: actions.INIT_REQUEST,
    payload,
  };
}

export function initSuccess(payload) {
  return {
    type: actions.INIT_SUCCESS,
    payload,
  };
}
