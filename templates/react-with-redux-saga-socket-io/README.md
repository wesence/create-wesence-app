# React with Redux Saga and Socket.io

Quick setup for new performance orientated, offline–first React.js applications.

## Documentation

### Setup

Run the setup to get everything up and running

```
npm install
```

And start the development server

```
npm start
```

Now you can go to http://localhost:3000 and see your app!

### Environment variables

Create a `.env` in the root directory and set the following variables. These variables are available in global `config`.

```
APP_NAME=
NODE_ENV=
API_BASE=
```

You can also add `PORT` variable to if you want a different port.

### Scripts

| Script          | Description                                     |
| --------------- | ----------------------------------------------- |
| `npm start`     | Runs project in `development` environment       |
| `npm run build` | Builds the project for `production` environment |
| `npm run test`  | Runs the test cases                             |
| `npm run clean` | Cleans the `build` directory                    |

### Folder structure

```
.
├── config                    # All config-related code
├── lib                       # All third library wrappers and initialization
├── public                    # Static public assets (not imported anywhere in source code)
├── src                       # Application source code
│   ├── components            # Global Reusable Components
│   ├── modules               # Components that dictate major functionalities
│   ├── pages                 # Main route definitions and split points
│   ├── store                 # Redux-specific pieces
│   │   ├── createStore.js    # Create and instrument redux store
│   │   ├── reducers.js       # Reducer registry and injection
│   │   └── rootSaga.js       # Application root saga
│   ├── styles                # Application-wide styles
│   │   └── theme.js          # Default style-specific pieces
│   ├── utils                 # Application-wide utilities
│   │   ├── actionCreator.js  # Action creator utility
│   │   └── codeSplitting.js  # Code splitting utility
│   ├── index.html            # Main HTML page container
│   └── main.jsx              # Application bootstrap and rendering
└── webpack                   # Webpack-specific pieces
```

### Developer guidelines

#### 1. Stucturing the codebase

##### Modules

- Are concerned with functionalities
- Contains components, container, store, services, utilities, etc.

##### Pages

- Composition of modules
- Are concerned with routes
- Are main split points

##### Components

- Are concerned with how things look.
- Don't specify how the data is loaded
- Receive data and callbacks exclusively via props.
- Rarely have their own state (when they do, it’s UI state rather than data).
- React components that are driven solely by props and don't talk to Redux. Same as “dumb components”. They should stay the same regardless of your router, data fetching library, etc.

##### Containers

- Are concerned with how things work.
- Provide the data and behavior to presentational or other container components.
- Call actions and provide these as callbacks to components
- Most of the type are stateful
- Are generated using HOC component()
- are aware of Redux, Router, etc

#### 2. Code splitting

Use `createSplitPoint` from `utils/codeSplitting` which takes a required parameter `path` that describes the module path relative to `src/`.

Example use:

```
import createSplitPoint from 'utils/codeSplitting';

export default createSplitPoint('modules/ExampleModule');
```

#### 3. Redux action creator

Use `actionCreator` from `utils/actionCreator` to ceate standard Redux action types. This takes Redux module name and an array of constants and returns an object containing all the action types.

Example use:

```
import actionCreator from 'utils/actionCreator';

const actions = actionCreator('crud-module', [
  'CREATE',
  'READ',
  'UPDATE'
  'DELETE',
]);
```

#### 4. Using `socket.io` with Redux

##### Emit an event

Dispatch `emitSocketIOEvent` action from `SocketIO` module to emit any socket event. This takes an socket event and payload as arguments.

Example use:

```
dispatch(emitSocketIOEvent('event', 'payload'));
```

##### Create connection

Use `createSocketIOConnection` from `utils/socket.io`, this will return a `socket.io` client.

Example use:

```
import createSocketIOConnection from 'utils/socket.io';

const socket = yield createSocketIOConnection();
```

##### Listen to socket events

Use `createChannel` and `eventListener` `utils/socket.io/helpers` to create channel and listen to socket events.

1. Create an event channel using `createChannel` that takes an `event` and a `callback`. The `callback` recieves event payload and an event emitter as arguments. Use the event emitter to dispatch any redux action in the callback.
2. Create a event listener saga using `eventListener` that takes the event channel as argument.

Example use:

```
import { createChannel, eventListener } from 'utils/socket.io/helpers';

// 1. Create channel
const channel = yield createChannel('some-event', (payload, emit) => {
  // Dispatch any redux action
  emit(someAction(payload));
  ...
});

// 2. Create event listener
const eventListenerSaga = yield eventListener(channel);
...
yield call(eventListenerSaga);
```

## Todo

These things are either not implemented or needs modification.

- Saga creator with CRUD operations
- Fix code splitting module path resolve, check `utils/codeSplitting.js`
- Clear Redux state on successful logout, check `store/rootReducer.js`
- Setup global styles for the app

## Bug report

If something doesn’t work, please [file an issue](https://bitbucket.org/wesence/frontend-boilerplate/issues/new).
