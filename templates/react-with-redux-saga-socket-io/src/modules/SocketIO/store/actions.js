import actionCreator from 'utils/actionCreator';

export const actions = actionCreator('socket-io', ['EMIT_SOCKET_IO_EVENT']);

export function emitSocketIOEvent(event, payload) {
  return {
    type: actions.EMIT_SOCKET_IO_EVENT,
    event,
    payload,
  };
}
