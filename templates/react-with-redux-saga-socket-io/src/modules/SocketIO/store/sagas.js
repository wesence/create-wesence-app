import { fork, takeLatest } from 'redux-saga/effects';
import createSocketIOConnection from 'utils/socket.io';
import { actions } from './actions';

// Event emitter
function* emitSocketIOEvent(socket, { event, payload }) {
  socket.emit(event, payload);
}

// Watch socket.io emit actions
function* emitSocketIOEventSaga(socket) {
  yield takeLatest(actions.EMIT_SOCKET_IO_EVENT, emitSocketIOEvent, socket);
}

// Handle socket.io
export function* handleSocketIOSaga() {
  const socket = yield createSocketIOConnection();

  yield fork(emitSocketIOEventSaga, socket);
}
