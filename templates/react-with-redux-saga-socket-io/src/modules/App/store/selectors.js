import { createSelector } from 'reselect';

const appState = (state) => state.app;

export const appSelector = createSelector(appState, (app) => app);
