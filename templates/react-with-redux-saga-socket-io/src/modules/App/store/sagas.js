import { put, takeLatest } from 'redux-saga/effects';
import { actions, initSuccess } from './actions';

function* initWorkerSaga({ payload }) {
  yield put(initSuccess(payload));
}

export function* initWatcherSaga() {
  yield takeLatest(actions.INIT_REQUEST, initWorkerSaga);
}
