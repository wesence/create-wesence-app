// @flow
import * as React from 'react';
import { Wrapper, H1 } from './App.styled';

type Props = {
  message: string,
};

const App = (props: Props) => (
  <Wrapper>
    <H1>React with Redux Saga</H1>
    <p>{props.message}</p>
  </Wrapper>
);

export default App;
