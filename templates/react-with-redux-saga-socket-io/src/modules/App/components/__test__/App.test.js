/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from 'styled-components';
import theme from 'styles/theme';
import App from '../App';

test('Compare snapshots', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <App message="Hello, world!" />
      </ThemeProvider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
