import { put, take, cancelled } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import createSocketIOConnection from 'utils/socket.io';

// generic channel creator
export function* createChannel(event, callback) {
  const socket = yield createSocketIOConnection();

  return eventChannel((emit) => {
    socket.on(event, (payload) => {
      callback(payload, emit); // returns payload and event emitter
    });

    return () => {};
  });
}

// generic event listener
export function* eventListener(channel) {
  try {
    while (true) {
      const action = yield take(channel);
      yield put(action); // dispatch the action returned by event emitter
    }
  } finally {
    if (yield cancelled()) {
      channel.close();
    }
  }
}
