/* global config */
import io from 'socket.io-client';
import { isEmpty } from 'lodash';
import { select } from 'redux-saga/effects';

let socket;

function* registerUser() {
  if (socket.registered) {
    return;
  }

  const user = yield select((state) => state.user.data);

  if (isEmpty(user)) {
    return;
  }

  socket.emit('register', user.id);
  socket.registered = true;
}

export default function* createSocketIOConnection() {
  if (socket && socket.connected) {
    yield registerUser();

    return socket;
  }

  socket = io(config.SOCKET_URL);

  yield registerUser();

  return socket;
}

export function getSocketIOClient() {
  return socket;
}
