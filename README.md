# Create Wesence App

Please use the following commands on your systems to use the app generator.

**Recommended:**

1. npm i -g https://bitbucket.org/wesence/create-wesence-app.git
2. create-wesence-app

**Alternative:**

1. git clone https://bitbucket.org/wesence/create-wesence-app.git
2. cd create-wesence-app
3. npm i -g
4. create-wesence-app

The generated app will be live on http://localhost:3000. Please check the `README` of that app for more configuration changes.

**Note:** You may need root access to install gloabally.

**Bugs** https://bitbucket.org/wesence/create-wesence-app/issues/new
