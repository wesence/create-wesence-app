"use strict";

const inquirer = require("inquirer");
const fs = require("fs");
const chalk = require("chalk");
const exec = require("child_process").exec;

const TEMPLATES = fs.readdirSync(`${__dirname}/templates`);

const QUESTIONS = [
  {
    type: "list",
    name: "template-choice",
    message: "What project template you would like to generate?",
    choices: [
      {
        name: "React with Redux Saga",
        value: "react-with-redux-saga"
      },
      {
        name: "React with Redux Saga and Socket.io",
        value: "react-with-redux-saga-socket-io"
      }
    ]
  },
  {
    type: "input",
    name: "project-name",
    message: "Project name:",
    validate: function(input) {
      if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
      else
        return "Project name may only include letters, numbers, underscores and hashes.";
    }
  }
];

const CURR_DIR = process.cwd();

function createDirectoryContents(templatePath, newProjectPath) {
  const filesToCreate = fs.readdirSync(templatePath);

  filesToCreate.forEach(file => {
    const origFilePath = `${templatePath}/${file}`;

    // rename
    if (file === ".npmignore") file = ".gitignore";

    // get stats about the current file
    const stats = fs.statSync(origFilePath);

    if (stats.isFile()) {
      const contents = fs.readFileSync(origFilePath, "utf8");

      const writePath = `${CURR_DIR}/${newProjectPath}/${file}`;
      fs.writeFileSync(writePath, contents, "utf8");
    } else if (stats.isDirectory()) {
      fs.mkdirSync(`${CURR_DIR}/${newProjectPath}/${file}`);

      // recursive call
      createDirectoryContents(
        `${templatePath}/${file}`,
        `${newProjectPath}/${file}`
      );
    }
  });
}

inquirer.prompt(QUESTIONS).then(answers => {
  const templateChoice = answers["template-choice"];
  const projectName = answers["project-name"];
  const templatePath = `${__dirname}/templates/${templateChoice}`;
  const projectDir = `${CURR_DIR}/${projectName}`;

  // create project directory
  fs.mkdir(projectDir, err => {
    if (err) {
      console.log(
        chalk.red(
          "\nUnable to create " + projectDir + ". Directory already exists."
        )
      );
      process.exit(1);
    }
  });

  // copy template to project directory
  console.log(`\nCreating ${projectName}...`);

  createDirectoryContents(templatePath, projectName);

  // install packages
  console.log("\nInstalling project dependencies...");

  exec(`cd ${projectDir} && npm install`, (err, stdout, stderr) => {
    if (err) {
      console.log(
        chalk.red(
          "Unable to install project dependencies.\n" +
            "Please install them manually."
        )
      );
      process.exit(1);
    }

    // print a message
    console.log(
      "\nSuccess! Created " +
        projectName +
        " in " +
        projectDir +
        "\n\n" +
        "Inside that directory, you can run several commands.\n\n" +
        chalk.cyan("  npm start") +
        "\n" +
        "    Starts the development server." +
        "\n\n" +
        chalk.cyan("  npm run build") +
        "\n" +
        "    Bundles the app into static files for production." +
        "\n\n" +
        chalk.cyan("  npm run test") +
        "\n" +
        "    Starts the test runner.\n"
    );

    console.log(
      "We suggest that you begin by typing:\n\n" +
        chalk.cyan("  cd ") +
        projectName +
        "\n" +
        chalk.cyan("  npm start") +
        "\n\n" +
        "Happy coding!"
    );
  });
});
